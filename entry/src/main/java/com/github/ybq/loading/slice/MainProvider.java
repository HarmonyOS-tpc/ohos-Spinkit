package com.github.ybq.loading.slice;

import com.github.ybq.core.sprite.Sprite;
import ohos.agp.colors.RgbColor;
import ohos.agp.components.*;
import ohos.agp.components.element.ShapeElement;
import ohos.app.Context;

import java.util.ArrayList;

public class MainProvider extends RecycleItemProvider {

    private ArrayList<Sprite> lists;
    private Context context;

    private int[] colors = new int[]{
            0XFFD55400,
            0XFF2B3E51,
            0XFF00BD9C,
            0XFF227FBB,
            0XFF7F8C8D,
            0XFFFFCC5C,
            0XFFD55400,
            0XFF1AAF5D,
    };
    public MainProvider(Context context,ArrayList<Sprite> list){
        this.context = context;
        this.lists = list;
    }

    @Override
    public int getCount() {
        return lists.size();
    }

    @Override
    public Object getItem(int i) {
        return lists.get(i);
    }

    @Override
    public long getItemId(int i) {
        return lists.get(i).hashCode();
    }

    @Override
    public Component getComponent(int i, Component component, ComponentContainer componentContainer) {
        DirectionalLayout container = (DirectionalLayout) LayoutScatter.getInstance(context).parse(com.github.ybq.loading.ResourceTable.Layout_layout_item,componentContainer,false);

        ShapeElement element = new ShapeElement();
        element.setRgbColor(RgbColor.fromArgbInt(colors[(i % colors.length)]));
        container.setBackground(element);
        lists.get(i).onBoundsChange(0,0,container.getWidth(),container.getHeight());
        lists.get(i).setComponent(container);

        container.addDrawTask((component1, canvas) -> lists.get(i).drawToCanvas(canvas));
        return container;
    }
}
