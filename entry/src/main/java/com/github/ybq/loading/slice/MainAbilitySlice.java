package com.github.ybq.loading.slice;


import com.github.ybq.core.sprite.Sprite;
import com.github.ybq.core.style.*;
import com.github.ybq.loading.ResourceTable;
import ohos.aafwk.ability.AbilitySlice;
import ohos.aafwk.content.Intent;
import ohos.agp.components.*;

import java.util.ArrayList;

public class MainAbilitySlice extends AbilitySlice {


    @Override
    public void onStart(Intent intent) {
        super.onStart(intent);
        super.setUIContent(ResourceTable.Layout_ability_main);


        Text text_style = (Text) findComponentById(ResourceTable.Id_text_style);
        Text text_widget = (Text) findComponentById(ResourceTable.Id_text_widget);
        ListContainer listContainer = (ListContainer) findComponentById(ResourceTable.Id_list_container);
        TableLayoutManager layoutManager = new TableLayoutManager();
        layoutManager.setColumnCount(4);
        listContainer.setLayoutManager(layoutManager);

        DirectionalLayout directionalLayout = (DirectionalLayout) findComponentById(ResourceTable.Id_dl_widget);
        ProgressBar progressBar = (ProgressBar) findComponentById(ResourceTable.Id_pb);
        Text text = (Text) findComponentById(ResourceTable.Id_tv);
        Button button = (Button) findComponentById(ResourceTable.Id_bt);
        Image image = (Image) findComponentById(ResourceTable.Id_iv);
        MainProvider provider = new MainProvider(this.getContext(),getLists());

        listContainer.setItemProvider(provider);


        text_style.setClickedListener(new Component.ClickedListener() {
            @Override
            public void onClick(Component component) {
                listContainer.setVisibility(Component.VISIBLE);
                directionalLayout.setVisibility(Component.HIDE);
            }
        });
        text_widget.setClickedListener(new Component.ClickedListener() {
            @Override
            public void onClick(Component component) {
                listContainer.setVisibility(Component.HIDE);
                directionalLayout.setVisibility(Component.VISIBLE);
            }
        });



        DoubleBounce doubleBounce = new DoubleBounce();
        doubleBounce.setPaintColor(0XFF1AAF5D);
        doubleBounce.onBoundsChange(0, 0, progressBar.getWidth(), progressBar.getHeight());
        doubleBounce.setComponent(progressBar);
        progressBar.setProgressElement(doubleBounce);
        progressBar.setIndeterminate(true);
        progressBar.addDrawTask((component, canvas) -> doubleBounce.drawToCanvas(canvas));

        Wave wave = new Wave();
        wave.setBounds(0, 0, button.getHeight(), button.getHeight());
        wave.setComponent(button);
        button.addDrawTask((component, canvas) -> wave.drawToCanvas(canvas));

        Circle circle = new Circle();
        circle.onBoundsChange( 0, 0, text.getHeight(), text.getHeight());
        circle.setComponent(text);
//        text.setAroundElements(null,null,circle,null);
        text.addDrawTask((component, canvas) -> circle.drawToCanvas(canvas));

        ChasingDots chasingDots = new ChasingDots();
        chasingDots.setBounds(0, 0, image.getWidth(), image.getHeight());
        chasingDots.setComponent(image);
        image.addDrawTask((component, canvas) -> chasingDots.drawToCanvas(canvas));
    }

    private ArrayList<Sprite> getLists(){
        ArrayList<Sprite> lists = new ArrayList<>();
        lists.add(new RotatingPlane());
        lists.add(new DoubleBounce());
        lists.add(new Wave());
        lists.add(new WanderingCubes());
        lists.add(new Pulse());
        lists.add(new ChasingDots());
        lists.add(new ThreeBounce());
        lists.add(new Circle());
        lists.add(new CubeGrid());
        lists.add(new FadingCircle());
        lists.add(new FoldingCube());
        lists.add(new RotatingCircle());
        lists.add(new MultiplePulse());
        lists.add(new PulseRing());
        lists.add(new MultiplePulseRing());

        return lists;
    }

    @Override
    public void onActive() {
        super.onActive();
    }

    @Override
    public void onForeground(Intent intent) {
        super.onForeground(intent);
    }
}
