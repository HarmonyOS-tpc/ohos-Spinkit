# ohos-SpinKit
>openharmony loading animations(I wrote a harmony edition according [SpinKit](https://tobiasahlin.com/spinkit))


## Preview

<img src="art/screen.gif" width="240px" height="240px"/>

<img src="art/screen2.gif" width="200px" height="200px"/>

## Gradle Dependency

 ``` groovy
dependencies {
    implementation 'io.openharmony.tpc.thirdlib:ohos-SpinKit:1.0.3'
 }
 ```


## Usage

- ProgressBar

 ```java
        DoubleBounce doubleBounce = new DoubleBounce();
        doubleBounce.setPaintColor(0XFF1AAF5D);
        doubleBounce.onBoundsChange(0, 0, progressBar.getWidth(), progressBar.getHeight());
        doubleBounce.setComponent(progressBar);
        progressBar.setProgressElement(doubleBounce);
        progressBar.setIndeterminate(true);
        progressBar.addDrawTask((component, canvas) -> doubleBounce.drawToCanvas(canvas));
 ```

## Style
> 

Style | Preview
------------     |   -------------
RotatingPlane    | <img src='art/RotatingPlane.gif' alt='RotatingPlane' width="90px" height="90px"/>
DoubleBounce     | <img src='art/DoubleBounce.gif' alt='DoubleBounce' width="90px" height="90px"/>
Wave             | <img src='art/Wave.gif' alt='Wave' width="90px" height="90px"/>
WanderingCubes   | <img src='art/WanderingCubes.gif' alt='WanderingCubes' width="90px" height="90px"/>
Pulse            | <img src='art/Pulse.gif' alt='Pulse' width="90px" height="90px"/>
ChasingDots      | <img src='art/ChasingDots.gif' alt='ChasingDots' width="90px" height="90px"/>
ThreeBounce      | <img src='art/ThreeBounce.gif' alt='ThreeBounce' width="90px" height="90px"/>
Circle           | <img src='art/Circle.gif' alt='Circle' width="90px" height="90px"/>
CubeGrid         | <img src='art/CubeGrid.gif' alt='CubeGrid' width="90px" height="90px"/>
FadingCircle     | <img src='art/FadingCircle.gif' alt='FadingCircle' width="90px" height="90px"/>
RotatingCircle   | <img src='art/RotatingCircle.gif' alt='RotatingCircle' width="90px" height="90px"/>







## Acknowledgements
- [SpinKit](https://gitee.com/openharmony-tpc/ohos-Spinkit).



