package com.github.ybq.core.style;

import com.github.ybq.core.sprite.Sprite;
import ohos.agp.render.Canvas;
import ohos.agp.render.Paint;
import ohos.agp.utils.Rect;
import ohos.eventhandler.EventHandler;
import ohos.eventhandler.EventRunner;
import ohos.eventhandler.InnerEvent;

import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

public class ThreeBounce extends Sprite {

    private int num = 3;
    private float[] radius = new float[num];
    private ScheduledExecutorService service = Executors.newScheduledThreadPool(num);
    private EventHandler handler;


    public ThreeBounce() {
        super();
        handler = new BounceHandler(EventRunner.getMainEventRunner());
    }

    private boolean isFirst = true;
    @Override
    protected void onDraw(Canvas canvas) {
        mPaint.setStyle(Paint.Style.FILL_STYLE);
        mPaint.setStrokeWidth(getPaintStrokeWidth());
        if (getRect() != null) {
            Rect bounds = clipSquare(getRect());
            float maxRadius = bounds.getWidth() / 8f;
            for (int i = 0; i < num; i++) {
                int left = bounds.getWidth() * i / 3 + bounds.left;
                float mRadiusValue = radius[i] * maxRadius;
                canvas.drawCircle(left + maxRadius, bounds.getCenterY(), mRadiusValue, mPaint);
            }
            startAnimator();
        }
    }
    @Override
    public void startAnimator(){
        if (isFirst){
            for (int i = 0; i < num; i++) {
                service.scheduleWithFixedDelay(new BounceRunnable(i),160 * i ,25 , TimeUnit.MILLISECONDS);
            }
            isFirst = false;
        }
    }
    @Override
    public boolean isRunning() {
        if (service != null){
            return !service.isShutdown();
        }
        return super.isRunning();
    }
    @Override
    public void stopAnimator(){
        if (service != null){
            if (!service.isShutdown()){
                service.shutdown();
            }
        }
    }
    private class BounceHandler extends EventHandler {

        BounceHandler(EventRunner runner) throws IllegalArgumentException {
            super(runner);
        }

        @Override
        protected void processEvent(InnerEvent event) {
            super.processEvent(event);
            float v = (float) event.object;
            int eventId = event.eventId;
            radius[eventId] = v > 0.5f ? (1 - v) * 2 : v * 2;
            if (getComponent() != null) {
                getComponent().invalidate();
            }
        }
    }

    private class BounceRunnable implements Runnable {

        private int index;
        private float v = 0;

        BounceRunnable(int index) {
            this.index = index;
        }

        @Override
        public void run() {
            if (v > 1f) {
                v = 0;
            }
            float value = getInterpolation(v);
            handler.sendEvent(InnerEvent.get(index, value));
            v += 0.018f;

        }

        private float getInterpolation(float t) {
            return t;
        }
    }

}
