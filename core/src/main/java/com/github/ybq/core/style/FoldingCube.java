package com.github.ybq.core.style;

import com.github.ybq.core.sprite.Sprite;
import ohos.agp.animation.Animator;
import ohos.agp.animation.AnimatorValue;
import ohos.agp.render.Canvas;
import ohos.agp.render.Paint;
import ohos.agp.render.ThreeDimView;
import ohos.agp.utils.Rect;
import ohos.agp.utils.RectFloat;

import java.util.Map;

public class FoldingCube extends Sprite {

    private static String TAG = "FoldingCube";

    private AnimatorValue mAnimatorValue;

    private float fraction;

    public FoldingCube(){
        super();
        initAnimator();
 
    }

    @Override
    protected void onDraw(Canvas canvas) {
        mPaint.setStyle(Paint.Style.FILL_STYLE);
        if (getRect() != null) {
            Rect bounds = clipSquare(getRect(),50);
            int size = Math.min(bounds.getWidth(), bounds.getHeight());

            int px = bounds.left + size / 2 + 1;
            int py = bounds.top + size / 2 + 1;

            int saveCount = canvas.save();
            canvas.rotate(45,px,py);

            for (int i = 0; i < 4; i++) {
                mPaint.setAlpha(1);
                if (fraction < (i + 1) * 0.25f && fraction >= i * 0.25f){
                    canvas.save();
                    canvas.translate(bounds.getWidth() / 2 + bounds.left, bounds.getHeight() / 2 - size / 4);

                    ThreeDimView threeDimView = new ThreeDimView();
                    threeDimView.rotateY(180 * (fraction - (i * 0.25f)) * 4);
                    threeDimView.applyToCanvas(canvas);
                    canvas.translate(-bounds.getWidth() / 2 - bounds.left, -bounds.getHeight() / 2 + size / 4);
                    mPaint.setAlpha(1 - (fraction - (i * 0.25f)) * 4);
                    canvas.drawRect(new RectFloat(bounds.left,
                            bounds.top,
                            px,
                            py), mPaint);
                    canvas.restore();
                }

                if(fraction < (i * 0.25f)){
                    canvas.drawRect(new RectFloat(bounds.left,
                            bounds.top,
                            px,
                            py), mPaint);
                }

                canvas.rotate(90,px,py);
            }

            canvas.restoreToCount(saveCount);

            startAnimator();
        }


    }
    @Override
    public void startAnimator(){
        if (mAnimatorValue != null){
            if (!mAnimatorValue.isRunning()){
                mAnimatorValue.start();
            }
        }
    }
    @Override
    public boolean isRunning() {
        if (mAnimatorValue != null){
            return mAnimatorValue.isRunning();
        }
        return super.isRunning();
    }
    @Override
    public void stopAnimator() {

    }

    private void initAnimator(){
        if (mAnimatorValue == null){
            mAnimatorValue = new AnimatorValue();
            mAnimatorValue.setLoopedCount(Animator.INFINITE);
            mAnimatorValue.setDuration(4000);
            mAnimatorValue.setCurveType(Animator.CurveType.LINEAR);
            mAnimatorValue.setValueUpdateListener(new AnimatorValue.ValueUpdateListener() {
                @Override
                public void onUpdate(AnimatorValue animatorValue, float v) {
                    fraction = v;
                    if (getComponent() != null){
                        getComponent().invalidate();
                    }
                }
            });
        }
    }
}

