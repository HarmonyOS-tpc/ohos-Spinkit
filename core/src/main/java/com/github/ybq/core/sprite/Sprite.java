package com.github.ybq.core.sprite;

import ohos.agp.components.Component;
import ohos.agp.components.element.ShapeElement;
import ohos.agp.render.Canvas;
import ohos.agp.render.Paint;
import ohos.agp.utils.Color;
import ohos.agp.utils.Rect;

public abstract class Sprite extends ShapeElement {
    private static String TAG = "Sprite";

    private Component component;

    private Rect mRect;

    protected Paint mPaint;
    private int paintColor = 0xffffffff;
    private float strokeWidth = 5f;

    public Sprite(){
        super();
        if (mPaint == null){
            mPaint = new Paint();
            mPaint.setColor(new Color(paintColor));
            mPaint.setStyle(Paint.Style.STROKE_STYLE);
            mPaint.setStrokeWidth(strokeWidth);
            mPaint.setAntiAlias(true);
        }
    }


    @Override
    public void drawToCanvas(Canvas canvas) {
        super.drawToCanvas(canvas);

        onDraw(canvas);
    }

    protected abstract void onDraw(Canvas canvas);
    public abstract void stopAnimator();
    public abstract void startAnimator();
    public boolean isRunning(){
        return false;
    }

    public void setStrokeWidth(float strokeWidth) {
        this.strokeWidth = strokeWidth;
        if (component != null){
            component.invalidate();
        }
    }

    public float getPaintStrokeWidth() {
        return strokeWidth;
    }

    public void setPaintColor(int paintColor) {
        this.paintColor = paintColor;
        if (mPaint != null){
            mPaint.setColor(new Color(paintColor));
        }
        if (component != null){
            component.invalidate();
        }
    }

    public int getPaintColor() {
        return paintColor;
    }

    public void onBoundsChange(Rect rectFloat){
        mRect = rectFloat;
        setBounds(rectFloat);
    }
    public void onBoundsChange(int left, int top, int right, int bottom) {
        super.setBounds(left, top, right, bottom);
        mRect = new Rect(left, top, right, bottom);
    }
    @Override
    public void setBounds(Rect bounds) {
        super.setBounds(bounds);
        mRect = bounds;
    }

    public Rect getRect() {
        return mRect;
    }

    @Override
    public void setBounds(int left, int top, int right, int bottom) {
        super.setBounds(left, top, right, bottom);
        mRect = new Rect(left, top, right, bottom);
    }



    public Rect clipSquare(Rect rect) {
        int w = rect.getWidth();
        int h = rect.getHeight();
        int min = Math.min(w, h);
        int cx = rect.getCenterX();
        int cy = rect.getCenterY();
        int r = min / 2;
        return new Rect(
                cx - r,
                cy - r,
                cx + r,
                cy + r
        );
    }
    public Rect clipSquare(Rect rect,int bound) {
        int w = rect.getWidth();
        int h = rect.getHeight();
        int min = Math.min(w, h);
        int cx = rect.getCenterX();
        int cy = rect.getCenterY();
        int r = min / 2;
        return new Rect(
                cx - r + bound,
                cy - r + bound,
                cx + r - bound,
                cy + r - bound
        );
    }
    public void setComponent(Component component) {
        this.component = component;
    }

    public Component getComponent() {
        return component;
    }
}

