package com.github.ybq.core;

import com.github.ybq.core.sprite.Sprite;
import ohos.agp.components.AttrSet;
import ohos.agp.components.ProgressBar;
import ohos.agp.components.element.Element;
import ohos.agp.utils.Color;
import ohos.app.Context;

public class SpinKitView extends ProgressBar {

    private Style mStyle;
    private int mColor;
    private Sprite mSprite;

    public SpinKitView(Context context) {
        this(context,null);
    }

    public SpinKitView(Context context, AttrSet attrSet) {
        this(context, attrSet,null);
    }

    public SpinKitView(Context context, AttrSet attrSet, String styleName) {
        super(context, attrSet, styleName);

        mStyle = Style.values()[AttrUtils.getIntFromAttr(attrSet,"spinKit_style",0)];
        mColor = AttrUtils.getColorFromAttr(attrSet,"spinKit_color", Color.WHITE.getValue());

        init();
    }
    private void init(){
        Sprite sprite = SpriteFactory.create(mStyle);
        sprite.setPaintColor(mColor);
        setInfiniteModeElement(sprite);
        setIndeterminate(true);
    }

    @Override
    public void setInfiniteModeElement(Element element) {
        if (!(element instanceof Sprite)){
            throw new IllegalArgumentException("this d must be instanceof Sprite");
        }
        setInfiniteModeElement((Sprite) element);
    }
    public void setInfiniteModeElement(Sprite d){
        super.setInfiniteModeElement(d);
        mSprite = d;
        if (mSprite.getPaintColor() == 0) {

            mSprite.setPaintColor(mColor);
        }
    }

    @Override
    public Sprite getInfiniteModeElement() {
        return mSprite;
    }
    public void setColor(int color) {
        this.mColor = color;
        if (mSprite != null) {

            mSprite.setPaintColor(color);
        }
        invalidate();
    }
}
