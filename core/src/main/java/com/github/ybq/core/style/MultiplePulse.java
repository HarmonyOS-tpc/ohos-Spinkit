package com.github.ybq.core.style;

import com.github.ybq.core.sprite.Sprite;
import ohos.agp.render.Canvas;
import ohos.agp.render.Paint;
import ohos.agp.utils.Rect;
import ohos.eventhandler.EventHandler;
import ohos.eventhandler.EventRunner;
import ohos.eventhandler.InnerEvent;

import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

public class MultiplePulse extends Sprite {

    private float[] radius = new float[3];
    private float[] alpha = new float[]{1f, 1f, 1f};

    private ScheduledExecutorService service = Executors.newScheduledThreadPool(3);
    private EventHandler handler;


    public MultiplePulse() {
        super();
        handler = new PulseHandler(EventRunner.getMainEventRunner());
    }

    private boolean isFirst = true;

    @Override
    protected void onDraw(Canvas canvas) {
        mPaint.setStyle(Paint.Style.FILL_STYLE);
        mPaint.setStrokeWidth(getPaintStrokeWidth());
        if (getRect() != null) {
            Rect bounds = clipSquare(getRect());
            float maxRadius = bounds.getWidth() / 2f - getPaintStrokeWidth() * 2;
            for (int i = 0; i < 3; i++) {
                float mRadiusValue = radius[i] * maxRadius;
                mPaint.setAlpha(alpha[i]);
                canvas.drawCircle(bounds.getCenterX(), bounds.getCenterY(), mRadiusValue, mPaint);
            }

            startAnimator();
        }
    }

    @Override
    public void startAnimator() {
        if (isFirst) {
            for (int i = 0; i < 3; i++) {
                service.scheduleWithFixedDelay(new PulseRunnable(i), 200 * (i + 1), 25, TimeUnit.MILLISECONDS);
            }
            isFirst = false;
        }
    }

    @Override
    public boolean isRunning() {
        if (service != null) {
            return !service.isShutdown();
        }
        return super.isRunning();
    }

    @Override
    public void stopAnimator() {
        if (service != null) {
            if (!service.isShutdown()) {
                service.shutdown();
            }
        }
    }

    private class PulseHandler extends EventHandler {

        PulseHandler(EventRunner runner) throws IllegalArgumentException {
            super(runner);
        }

        @Override
        protected void processEvent(InnerEvent event) {
            super.processEvent(event);
            float v = (float) event.object;
            int eventId = event.eventId;
            radius[eventId] = v;
            alpha[eventId] = 1 - v;

            if (getComponent() != null) {
                getComponent().invalidate();
            }
        }
    }

    private class PulseRunnable implements Runnable {

        private int index;
        private float v = 0;

        PulseRunnable(int index) {
            this.index = index;
        }

        @Override
        public void run() {
            if (v > 1f) {
                v = 0;
            }
            float value = getInterpolation(v);
            handler.sendEvent(InnerEvent.get(index, value));

            v += 0.01f;

        }

        private float getInterpolation(float t) {
            t -= 1.0f;
            return t * t * ((0.2f + 1) * t + 0.2f) + 1.0f;
        }
    }
}
