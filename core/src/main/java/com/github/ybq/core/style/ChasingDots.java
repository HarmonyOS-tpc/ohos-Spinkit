package com.github.ybq.core.style;

import com.github.ybq.core.sprite.Sprite;
import ohos.agp.render.Canvas;
import ohos.agp.render.Paint;
import ohos.agp.utils.Rect;
import ohos.eventhandler.EventHandler;
import ohos.eventhandler.EventRunner;
import ohos.eventhandler.InnerEvent;

import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

public class ChasingDots extends Sprite {

    private int num = 2;
    private float[] radius = new float[num];
    private float angle = 0f;
    private ScheduledExecutorService service = Executors.newScheduledThreadPool(num);
    private EventHandler handler;


    public ChasingDots() {
        super();
        handler = new ChasingHandler(EventRunner.getMainEventRunner());
    }

    private boolean isFirst = true;
    @Override
    protected void onDraw(Canvas canvas) {
        mPaint.setStyle(Paint.Style.FILL_STYLE);
        if (getRect() != null) {
            Rect bounds = clipSquare(getRect(),10);
            int drawW = (int) (bounds.getWidth() * 0.62);
            Rect rect1 = new Rect(
                    bounds.right - drawW,
                    bounds.top,
                    bounds.right,
                    bounds.top + drawW );
            Rect rect2 = new Rect(bounds.right - drawW ,
                    bounds.bottom - drawW,
                    bounds.right,
                    bounds.bottom );
            float radius1 = radius[0] * rect1.getWidth() / 2;
            float radius2 = radius[1] * rect2.getWidth() / 2;

            canvas.rotate(angle,bounds.getCenterX(),bounds.getCenterY());
            canvas.drawCircle(rect1.getCenterX(),rect1.getCenterY(),radius1,mPaint);
            canvas.drawCircle(rect2.getCenterX(),rect2.getCenterY(),radius2,mPaint);
            startAnimator();
        }
    }
    @Override
    public void startAnimator(){
        if (isFirst){
            for (int i = 0; i < num; i++) {
                long time = i == 0 ? 0 : 1000;
                service.scheduleWithFixedDelay(new ChasingRunnable(i),time ,15 , TimeUnit.MILLISECONDS);
            }
            isFirst = false;
        }
    }
    @Override
    public void stopAnimator(){
        if (service != null){
            if (!service.isShutdown()){
                service.shutdown();
            }
        }
    }
    @Override
    public boolean isRunning() {
        if (service != null){
            return !service.isShutdown();
        }
        return super.isRunning();
    }
    private class ChasingHandler extends EventHandler {

        ChasingHandler(EventRunner runner) throws IllegalArgumentException {
            super(runner);
        }

        @Override
        protected void processEvent(InnerEvent event) {
            super.processEvent(event);
            float v = (float) event.object;
            int eventId = event.eventId;
            radius[eventId] = v > 0.5 ? (1 - v) * 2 : v * 2;
            if (eventId == 0){
                angle = v * 360;
            }
            if (getComponent() != null) {
                getComponent().invalidate();
            }
        }
    }

    private class ChasingRunnable implements Runnable {

        private int index;
        private float v = 0;

        ChasingRunnable(int index) {
            this.index = index;
        }

        @Override
        public void run() {
            if (v > 1) {
                v = 0;
            }
            float value = getInterpolation(v);
            handler.sendEvent(InnerEvent.get(index, value));
            v += 0.0075;
        }

        private float getInterpolation(float t) {
            return t;
        }
    }
}