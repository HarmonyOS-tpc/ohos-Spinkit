package com.github.ybq.core.style;

import com.github.ybq.core.sprite.Sprite;
import ohos.agp.animation.Animator;
import ohos.agp.animation.AnimatorValue;
import ohos.agp.render.Canvas;
import ohos.agp.render.Paint;
import ohos.agp.utils.Rect;
import ohos.agp.utils.RectFloat;

public class WanderingCubes extends Sprite {

    private float radius = 1f;
    private float rotate = 0f;
    private float[] translate = new float[]{0,0};

    private AnimatorValue mAnimatorValue;

    public WanderingCubes(){
        super();
        initAnimator();
    }

    @Override
    protected void onDraw(Canvas canvas) {
        mPaint.setStyle(Paint.Style.FILL_STYLE);
        if (getRect() != null) {
            Rect bounds = clipSquare(getRect(),15);

            RectFloat aFloat = new RectFloat((float) bounds.left, (float) bounds.top, bounds.left + bounds.getWidth() / 4f, bounds.top + bounds.getHeight() / 4f);
            RectFloat bFloat = new RectFloat((float) bounds.right - bounds.getWidth() / 4f,(float) bounds.bottom - bounds.getHeight() / 4f,bounds.right,bounds.bottom);

            float translateX = bounds.getWidth() - aFloat.getWidth();

            float x = translate[0] * translateX;
            float y = translate[1] * translateX;

            aFloat.translate( x == 0 ? translateX : x,y);
            bFloat.translate(x == 0 ? -translateX : -x,-y);

            int save1 = canvas.save();
            canvas.rotate(rotate,aFloat.getHorizontalCenter(),aFloat.getVerticalCenter());
            canvas.scale(radius,radius,aFloat.getHorizontalCenter(),aFloat.getVerticalCenter());
            canvas.drawRect(aFloat,mPaint);
            canvas.restoreToCount(save1);


            int save2 = canvas.save();
            canvas.rotate(-rotate,bFloat.getHorizontalCenter(),bFloat.getVerticalCenter());
            canvas.scale(radius,radius,bFloat.getHorizontalCenter(),bFloat.getVerticalCenter());
            canvas.drawRect(bFloat,mPaint);
            canvas.restoreToCount(save2);

            startAnimator();
        }
    }
    @Override
    public void startAnimator(){
        if (mAnimatorValue != null){
            if (!mAnimatorValue.isRunning()){
                mAnimatorValue.start();
            }
        }
    }
    private void initAnimator(){
        if (mAnimatorValue == null){
            mAnimatorValue = new AnimatorValue();
            mAnimatorValue.setLoopedCount(Animator.INFINITE);
            mAnimatorValue.setDuration(1000);
            mAnimatorValue.setCurveType(Animator.CurveType.LINEAR);
            mAnimatorValue.setValueUpdateListener(new AnimatorValue.ValueUpdateListener() {
                @Override
                public void onUpdate(AnimatorValue animatorValue, float v) {
                    rotate = v * 180f;
                    radius = v > 0.5f ? v :  (1 - v*2) / 2f + 0.5f;

                        if (v > 0.5){
                            translate[0] = 0;
                            translate[1] = (v - 0.5f) * 2;
                        }else {
                            translate[0] = v * 2;
                            translate[1] = 0;
                        }

                    if (getComponent() != null){
                        getComponent().invalidate();
                    }
                }
            });
        }
    }
    @Override
    public boolean isRunning() {
        if (mAnimatorValue != null){
            return mAnimatorValue.isRunning();
        }
        return super.isRunning();
    }
    @Override
    public void stopAnimator(){
        if (mAnimatorValue != null){
            if (mAnimatorValue.isRunning()){
                mAnimatorValue.stop();
            }
        }
    }
}
