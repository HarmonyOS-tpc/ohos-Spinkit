package com.github.ybq.core.style;

import com.github.ybq.core.sprite.Sprite;
import ohos.agp.animation.Animator;
import ohos.agp.animation.AnimatorValue;
import ohos.agp.render.Canvas;
import ohos.agp.render.Paint;
import ohos.agp.utils.Rect;
import ohos.agp.utils.RectFloat;

public class Wave extends Sprite {

    private AnimatorValue mAnimatorValue;

    private int waveNum = 5;
    private RectFloat rectFloat = new RectFloat(0, 0, 0, 0);
    private float v = 0f;

    public Wave() {
        super();
        initAnimator();
    }

    @Override
    protected void onDraw(Canvas canvas) {
        mPaint.setStyle(Paint.Style.FILL_STYLE);
        if (getRect() != null) {
            Rect bounds = clipSquare(getRect(), 50);
            int rw = bounds.getWidth() / waveNum;
            int width = bounds.getWidth() / 5 * 3 / 5;
            for (int i = 0; i < waveNum; i++) {
                int l = bounds.left + i * rw + rw / 5;
                int r = l + width;

                float a = i / 10f;
                float x = v - a;

                if (x < 0) x += 1;
                float h = bounds.getHeight();

                float c = 0.5f;
                if (v == -1 || x > c) {
                    h = bounds.getHeight() * 0.4f;
                } else {
                    h = (float) (x < c / 2 ? (x / (c / 2) * h) : ((c - x) / (c / 2) * h)) * 0.6f + h * 0.4f;
                }

                float t = bounds.getCenterY() - h / 2;
                float b = bounds.getCenterY() + h / 2;

                rectFloat.modify(l, t, r, b);
                canvas.drawRect(rectFloat, mPaint);
            }

            startAnimator();
        }
    }

    @Override
    public void startAnimator() {
        if (mAnimatorValue != null) {
            if (!mAnimatorValue.isRunning()) {
                mAnimatorValue.start();
            }
        }
    }

    private void initAnimator() {
        if (mAnimatorValue == null) {
            mAnimatorValue = new AnimatorValue();
            mAnimatorValue.setLoopedCount(Animator.INFINITE);
            mAnimatorValue.setDuration(2000);
            mAnimatorValue.setCurveType(Animator.CurveType.LINEAR);
            mAnimatorValue.setValueUpdateListener(new AnimatorValue.ValueUpdateListener() {
                @Override
                public void onUpdate(AnimatorValue animatorValue, float v) {
                    Wave.this.v = v < 0.5 ? v * 2 : -1;
                    if (getComponent() != null) {
                        getComponent().invalidate();
                    }
                }
            });
        }
    }

    @Override
    public void stopAnimator() {
        if (mAnimatorValue != null) {
            if (mAnimatorValue.isRunning()) {
                mAnimatorValue.stop();
            }
        }
    }

    @Override
    public boolean isRunning() {
        if (mAnimatorValue != null) {
            return mAnimatorValue.isRunning();
        }
        return super.isRunning();
    }

    public void setWaveNum(int waveNum) {
        this.waveNum = waveNum;
        if (getComponent() != null) {
            getComponent().invalidate();
        }
    }


}
