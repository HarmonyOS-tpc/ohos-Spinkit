package com.github.ybq.core.style;

import com.github.ybq.core.sprite.Sprite;
import ohos.agp.render.Canvas;
import ohos.agp.render.Paint;
import ohos.agp.utils.Rect;
import ohos.agp.utils.RectFloat;
import ohos.eventhandler.EventHandler;
import ohos.eventhandler.EventRunner;
import ohos.eventhandler.InnerEvent;

import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

public class CubeGrid extends Sprite {

    private int num = 9;

    private ScheduledExecutorService service = Executors.newScheduledThreadPool(num);
    private EventHandler handler;

    private int[] delays = new int[]{
            200, 100, 0,
            300, 200, 100,
            400, 300, 200
    };
    private float[] radius = new float[num];
    public CubeGrid() {
        super();
        handler = new CubeGridHandler(EventRunner.getMainEventRunner());
    }

    private boolean isFirst = true;
    @Override
    protected void onDraw(Canvas canvas) {
        mPaint.setStyle(Paint.Style.FILL_STYLE);
        mPaint.setStrokeWidth(getPaintStrokeWidth());
        if (getRect() != null) {
            Rect bounds = clipSquare(getRect(),50);
            int width = (int) (bounds.getWidth() * 0.33f);
            int height = (int) (bounds.getHeight() * 0.33f);
            for (int i = 0; i < num; i++) {
                int x = i % 3;
                int y = i / 3;
                int l = bounds.left + x * width;
                int t = bounds.top + y * height;
                RectFloat max = new RectFloat(l,t,l+width,t+width);
                float r = max.getWidth() / 2f * radius[i];

                RectFloat now = new RectFloat(
                        max.getVerticalCenter() - r ,
                        max.getHorizontalCenter() - r ,
                        max.getVerticalCenter() + r ,
                        max.getHorizontalCenter() + r
                );

                canvas.drawRect(now,mPaint);
            }

            startAnimator();
        }
    }
    @Override
    public void startAnimator(){
        if (isFirst){
            for (int i = 0; i < num; i++) {
                service.scheduleWithFixedDelay(new CubeGridRunnable(i),delays[i] ,25 , TimeUnit.MILLISECONDS);
            }
            isFirst = false;
        }
    }
    @Override
    public boolean isRunning() {
        if (service != null){
            return !service.isShutdown();
        }
        return super.isRunning();
    }
    @Override
    public void stopAnimator(){
        if (service != null){
            if (!service.isShutdown()){
                service.shutdown();
            }
        }
    }
    private class CubeGridHandler extends EventHandler {

        CubeGridHandler(EventRunner runner) throws IllegalArgumentException {
            super(runner);
        }

        @Override
        protected void processEvent(InnerEvent event) {
            super.processEvent(event);
            float v = (float) event.object;
            int eventId = event.eventId;
            if (v < 0.25f){
                v = 1 - (v * 4);
            }else if (v >= 0.25 && v <= 0.5){
                v = (v- 0.25f) * 4;
            }else {
                v = 1f;
            }
            radius[eventId] = v;
            if (getComponent() != null) {
                getComponent().invalidate();
            }
        }
    }

    private class CubeGridRunnable implements Runnable {

        private int index;
        private float v = 0;

        CubeGridRunnable(int index) {
            this.index = index;
        }

        @Override
        public void run() {
            if (v > 1) {
                v = 0;
            }
            float value = getInterpolation(v);
            handler.sendEvent(InnerEvent.get(index, value));
            v += 0.019;
        }

        private float getInterpolation(float t) {
            return t;
        }
    }
}
