package com.github.ybq.core.style;

import com.github.ybq.core.sprite.Sprite;
import ohos.agp.animation.Animator;
import ohos.agp.animation.AnimatorValue;
import ohos.agp.render.Canvas;
import ohos.agp.render.Paint;
import ohos.agp.utils.Rect;

public class FadingCircle extends Sprite {

    private AnimatorValue mAnimatorValue;

    private int botNum = 12;


    private float jiaodu = 0;

    public FadingCircle(){
        super();
        initAnimator();
    }

    @Override
    protected void onDraw(Canvas canvas) {
        mPaint.setStyle(Paint.Style.FILL_STYLE);
        mPaint.setAntiAlias(false);
        mPaint.setAlpha(0f);
        if (getRect() != null){
            Rect bounds = clipSquare(getRect());
            float radius = (float) (bounds.getWidth() * Math.PI / 3.6f / botNum);
            float jiao = 360f / botNum;
            for (int i = 0; i < botNum; i++) {
                int count = canvas.save();
                canvas.rotate(i * 360f / botNum,
                        bounds.getCenterX(),
                        bounds.getCenterY());
                float alpha = 1f;
                float x = jiaodu - i*jiao;
                if(x < 0) x+= 360;
                alpha = x > 180?0: (180-(x))/180;

                mPaint.setAlpha(alpha);
                drawBot(canvas,i, radius,bounds);
                canvas.restoreToCount(count);
            }
            startAnimator();
        }
    }
    @Override
    public void startAnimator(){
        if (mAnimatorValue != null){
            if (!mAnimatorValue.isRunning()){
                mAnimatorValue.start();
            }
        }
    }
    @Override
    public boolean isRunning() {
        if (mAnimatorValue != null){
            return mAnimatorValue.isRunning();
        }
        return super.isRunning();
    }
    private void drawBot(Canvas canvas, int index, float radius, Rect bounds){
        canvas.drawCircle(bounds.getWidth()/2f,radius,radius,mPaint);
    }

    public void setBotNum(int botNum) {
        this.botNum = botNum;
        if (getComponent() != null){
            getComponent().invalidate();
        }
    }


    private void initAnimator(){
        if (mAnimatorValue == null){
            mAnimatorValue = new AnimatorValue();
            mAnimatorValue.setLoopedCount(Animator.INFINITE);
            mAnimatorValue.setDuration(1500);
            mAnimatorValue.setDelay(100);
            mAnimatorValue.setCurveType(Animator.CurveType.LINEAR);
            mAnimatorValue.setValueUpdateListener(new AnimatorValue.ValueUpdateListener() {
                @Override
                public void onUpdate(AnimatorValue animatorValue, float v) {
                    jiaodu = v * 360;

                    if (getComponent() != null){
                        getComponent().invalidate();
                    }
                }
            });
        }
    }
    @Override
    public void stopAnimator(){
        if (mAnimatorValue != null){
            if (mAnimatorValue.isRunning()){
                mAnimatorValue.stop();
            }
        }
    }

}
